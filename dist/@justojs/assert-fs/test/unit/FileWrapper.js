"use strict";

var _dogmalangmin = require("dogmalangmin");

var _justo = require("justo");

const assert = _dogmalangmin.dogma.use(require("../../../../@justojs/assert-fs"));
module.exports = exports = (0, _justo.suite)(__filename, () => {
  {
    (0, _justo.suite)("file()", () => {
      {
        (0, _justo.test)("file() - error", () => {
          {
            assert(() => {
              {
                assert.file();
              }
            }).raises("file expected.");
          }
        });(0, _justo.test)("file(file)", () => {
          {
            assert(assert.file("myfile")).is("FileWrapper").mem("_path").eq("myfile");
          }
        });(0, _justo.test)("file(dir, file)", () => {
          {
            assert(assert.file("/my/dir", "file.txt")).is("FileWrapper").mem("_path").eq("/my/dir/file.txt");
          }
        });
      }
    });(0, _justo.suite)("exists()", () => {
      {
        (0, _justo.test)("exists()", () => {
          {
            const f = assert.file("test/data/empty.txt");assert(f.exists()).sameAs(f);
          }
        });(0, _justo.test)("exists() - error, existing item but it not being file", () => {
          {
            assert(() => {
              {
                assert.file("test/data").exists();
              }
            }).raises("file 'test/data' must exist.");
          }
        });(0, _justo.test)("exists() - error, not existing item", () => {
          {
            assert(() => {
              {
                assert.file("unknown").exists();
              }
            }).raises("file 'unknown' must exist.");
          }
        });
      }
    });(0, _justo.suite)("notExists()", () => {
      {
        (0, _justo.test)("doesNotExist()", () => {
          {
            const f = assert.file("test/data");assert(f.doesNotExist()).sameAs(f);
          }
        });(0, _justo.test)("notExists() - item exists but not being file", () => {
          {
            const f = assert.file("test/data");assert(f.notExists()).sameAs(f);
          }
        });(0, _justo.test)("notExists() - item existing", () => {
          {
            assert(() => {
              {
                assert.file("test/data/empty.txt").notExists();
              }
            }).raises("file 'test/data/empty.txt' must not exist.");
          }
        });
      }
    });(0, _justo.suite)("includes()", () => {
      {
        const f = assert.file("test/data/file.txt");(0, _justo.test)("includes(text)", () => {
          {
            assert(f.includes("hola")).sameAs(f);
          }
        });(0, _justo.test)("includes(text) - error", () => {
          {
            assert(() => {
              {
                f.includes("unknown");
              }
            }).raises("file 'test/data/file.txt' must include 'unknown'.");
          }
        });(0, _justo.test)("includes(num)", () => {
          {
            assert(f.includes(123)).sameAs(f);
          }
        });(0, _justo.test)("includes(num) - error", () => {
          {
            assert(() => {
              {
                f.includes(456);
              }
            }).raises("file 'test/data/file.txt' must include '456'.");
          }
        });(0, _justo.test)("includes(bool)", () => {
          {
            assert(f.includes(true)).sameAs(f);
          }
        });(0, _justo.test)("includes(bool) - error", () => {
          {
            assert(() => {
              {
                f.includes(false);
              }
            }).raises("file 'test/data/file.txt' must include 'false'.");
          }
        });(0, _justo.test)("includes(list)", () => {
          {
            assert(f.includes(["ciao", "hola"])).sameAs(f);
          }
        });(0, _justo.test)("includes(list) - error when none", () => {
          {
            assert(() => {
              {
                f.includes(["unknown", "none"]);
              }
            }).raises("file 'test/data/file.txt' must include 'unknown'.");
          }
        });(0, _justo.test)("includes(list) - error when someone", () => {
          {
            assert(() => {
              {
                f.includes(["hola", "unknown", "ciao"]);
              }
            }).raises("file 'test/data/file.txt' must include 'unknown'.");
          }
        });
      }
    });(0, _justo.suite)("notIncludes()", () => {
      {
        const f = assert.file("test/data/file.txt");(0, _justo.test)("doesNotInclude(text)", () => {
          {
            assert(f.doesNotInclude("unknown")).sameAs(f);
          }
        });(0, _justo.test)("notIncludes(text)", () => {
          {
            assert(f.notIncludes("unknown")).sameAs(f);
          }
        });(0, _justo.test)("notIncludes(text) - error", () => {
          {
            assert(() => {
              {
                f.notIncludes("hello");
              }
            }).raises("file 'test/data/file.txt' must not include 'hello'.");
          }
        });(0, _justo.test)("notIncludes(num)", () => {
          {
            assert(f.notIncludes(456)).sameAs(f);
          }
        });(0, _justo.test)("notIncludes(num) - error", () => {
          {
            assert(() => {
              {
                f.notIncludes(123);
              }
            }).raises("file 'test/data/file.txt' must not include '123'.");
          }
        });(0, _justo.test)("notIncludes(bool)", () => {
          {
            assert(f.notIncludes(false)).sameAs(f);
          }
        });(0, _justo.test)("notIncludes(bool) - error", () => {
          {
            assert(() => {
              {
                f.notIncludes(true);
              }
            }).raises("file 'test/data/file.txt' must not include 'true'.");
          }
        });(0, _justo.test)("notIncludes(list)", () => {
          {
            assert(f.notIncludes(["unk", "nown"])).sameAs(f);
          }
        });(0, _justo.test)("notIncludes(list) - error when everyone", () => {
          {
            assert(() => {
              {
                f.notIncludes(["hello", "ciao"]);
              }
            }).raises("file 'test/data/file.txt' must not include 'hello'.");
          }
        });(0, _justo.test)("notIncludes(list) - error when someone", () => {
          {
            assert(() => {
              {
                f.notIncludes(["hola", "unknown", "ciao"]);
              }
            }).raises("file 'test/data/file.txt' must not include 'hola'.");
          }
        });
      }
    });(0, _justo.suite)("isEmpty()", () => {
      {
        (0, _justo.test)("isEmpty()", () => {
          {
            const f = assert.file("test/data/empty.txt");assert(f.isEmpty()).sameAs(f);
          }
        });(0, _justo.test)("isEmpty() - error", () => {
          {
            assert(() => {
              {
                assert.file("test/data/file.txt").isEmpty();
              }
            }).raises("file 'test/data/file.txt' must be empty.");
          }
        });
      }
    });(0, _justo.suite)("isNotEmpty()", () => {
      {
        (0, _justo.test)("isNotEmpty()", () => {
          {
            const f = assert.file("test/data/file.txt");assert(f.isNotEmpty()).sameAs(f);
          }
        });(0, _justo.test)("isNotEmpty() - error", () => {
          {
            assert(() => {
              {
                assert.file("test/data/empty.txt").isNotEmpty();
              }
            }).raises("file 'test/data/empty.txt' must not be empty.");
          }
        });
      }
    });(0, _justo.suite)("eq()", () => {
      {
        (0, _justo.test)("eq(text)", () => {
          {
            const f = assert.file("test/data/eq.txt");assert(f.eq("ciao mondo!\n")).sameAs(f);
          }
        });(0, _justo.test)("eq(text) - error", () => {
          {
            assert(() => {
              {
                assert.file("test/data/eq.txt").eq("ciao mondo!");
              }
            }).raises("'test/data/eq.txt' file content must be 'ciao mondo!'.");
          }
        });
      }
    });(0, _justo.suite)("ne()", () => {
      {
        (0, _justo.test)("ne(text)", () => {
          {
            const f = assert.file("test/data/eq.txt");assert(f.neq("hola mundo!\n")).sameAs(f);
          }
        });(0, _justo.test)("ne(text) - error", () => {
          {
            assert(() => {
              {
                assert.file("test/data/eq.txt").ne("ciao mondo!\n");
              }
            }).raises("'test/data/eq.txt' file content must be 'ciao mondo!\n'.");
          }
        });
      }
    });(0, _justo.suite)("sameAs()", () => {
      {
        const f = assert.file("test/data/sameAs1.txt");(0, _justo.test)("sameAs(text)", () => {
          {
            assert(f.sameAs("test/data/sameAs2.txt")).sameAs(f);
          }
        });(0, _justo.test)("sameAs(text) - error", () => {
          {
            assert(() => {
              {
                f.sameAs("test/data/file.txt");
              }
            }).raises("file 'test/data/sameAs1.txt' must be same as file 'test/data/file.txt'.");
          }
        });
      }
    });(0, _justo.suite)("notSameAs()", () => {
      {
        const f = assert.file("test/data/sameAs1.txt");(0, _justo.test)("notSameAs(text)", () => {
          {
            assert(f.notSameAs("test/data/file.txt")).sameAs(f);
          }
        });(0, _justo.test)("notSameAs(text) - error", () => {
          {
            assert(() => {
              {
                f.notSameAs("test/data/sameAs2.txt");
              }
            }).raises("file 'test/data/sameAs1.txt' must not be same as file 'test/data/sameAs2.txt'.");
          }
        });
      }
    });(0, _justo.suite)("isJson()", () => {
      {
        (0, _justo.test)("ok", () => {
          {
            const f = assert.file("test/data/my.json");assert(f.isJson()).sameAs(f);
          }
        });(0, _justo.test)("error", () => {
          {
            assert(() => {
              {
                assert.file("test/data/empty.txt").isJson();
              }
            }).raises("file 'test/data/empty.txt' must be JSON.");
          }
        });
      }
    });
  }
});