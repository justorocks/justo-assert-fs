"use strict";

var _dogmalangmin = require("dogmalangmin");

var _justo = require("justo");

const assert = _dogmalangmin.dogma.use(require("../../../../@justojs/assert-fs"));
module.exports = exports = (0, _justo.suite)(__filename, () => {
  {
    (0, _justo.test)("assert()", () => {
      {
        assert(assert).mem("file").isFn().mem("dir").isFn();
      }
    });
  }
});