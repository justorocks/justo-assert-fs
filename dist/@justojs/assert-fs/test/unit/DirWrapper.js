"use strict";

var _dogmalangmin = require("dogmalangmin");

var _justo = require("justo");

const assert = _dogmalangmin.dogma.use(require("../../../../@justojs/assert-fs"));
module.exports = exports = (0, _justo.suite)(__filename, () => {
  {
    (0, _justo.suite)("dir()", () => {
      {
        (0, _justo.test)("dir() - error when dir expected", () => {
          {
            assert(() => {
              {
                assert.dir();
              }
            }).raises("dir expected.");
          }
        });(0, _justo.test)("dir(dir)", () => {
          {
            assert(assert.dir("test/data")).is("DirWrapper").mem("_path").eq("test/data");
          }
        });(0, _justo.test)("dir(parent, name)", () => {
          {
            assert(assert.dir("test", "data")).isNotNil().mem("_path").eq("test/data");
          }
        });
      }
    });(0, _justo.suite)("exists()", () => {
      {
        (0, _justo.test)("exists()", () => {
          {
            const dir = assert.dir("test", "data");assert(dir.exists()).sameAs(dir);
          }
        });(0, _justo.test)("exists() - error when not existing", () => {
          {
            assert(() => {
              {
                assert.dir("test", "unknown").exists();
              }
            }).raises("dir 'test/unknown' must exist.");
          }
        });(0, _justo.test)("exists() - error when dir is file", () => {
          {
            assert(() => {
              {
                assert.dir("test", "data", "file.txt").exists();
              }
            }).raises("dir 'test/data/file.txt' must exist.");
          }
        });
      }
    });(0, _justo.suite)("notExists()", () => {
      {
        (0, _justo.test)("doesNotExist()", () => {
          {
            const dir = assert.dir("test", "unknown");assert(dir.doesNotExist()).sameAs(dir);
          }
        });(0, _justo.test)("notExists()", () => {
          {
            const dir = assert.dir("test", "unknown");assert(dir.notExists()).sameAs(dir);
          }
        });(0, _justo.test)("notExists() - when dir is file", () => {
          {
            const dir = assert.dir("test", "data", "file.txt");assert(dir.notExists()).sameAs(dir);
          }
        });(0, _justo.test)("notExists() - error when existing", () => {
          {
            assert(() => {
              {
                assert.dir("test", "data").notExists();
              }
            }).raises("dir 'test/data' must not exist.");
          }
        });
      }
    });(0, _justo.suite)("has()", () => {
      {
        (0, _justo.test)("has(entry:text) - when file", () => {
          {
            const dir = assert.dir("test", "data");assert(dir.has("empty.txt")).sameAs(dir);
          }
        });(0, _justo.test)("has(entry:text) - when dir", () => {
          {
            const dir = assert.dir("test");assert(dir.has("data")).sameAs(dir);
          }
        });(0, _justo.test)("has(entry:text) - error", () => {
          {
            assert(() => {
              {
                assert.dir("test").has("unknown.txt");
              }
            }).raises("dir 'test' must have entry 'unknown.txt'.");
          }
        });(0, _justo.test)("has(entry:list)", () => {
          {
            const dir = assert.dir("test", "data");assert(dir.has(["empty.txt"])).sameAs(dir);
          }
        });
      }
    });(0, _justo.suite)("notHas()", () => {
      {
        (0, _justo.test)("doesNotHave(entry)", () => {
          {
            const dir = assert.dir("test", "data");assert(dir.doesNotHave("unknown.txt")).sameAs(dir);
          }
        });(0, _justo.test)("notHas(entry:list)", () => {
          {
            const dir = assert.dir("test", "data");assert(dir.notHas(["unknown.txt"])).sameAs(dir);
          }
        });(0, _justo.test)("notHas(entry:text) - error when entry existing", () => {
          {
            assert(() => {
              {
                assert.dir("test").notHas("data");
              }
            }).raises("dir 'test' must not have entry 'data'.");
          }
        });(0, _justo.test)("notHas(entry:text) - error when file instead of dir", () => {
          {
            assert(() => {
              {
                assert.dir("test/data/empty.txt").notHas("empty.txt");
              }
            }).raises("dir 'test/data/empty.txt' must exist.");
          }
        });(0, _justo.test)("notHas(entry:text) - error when non-existing dir", () => {
          {
            assert(() => {
              {
                assert.dir("test/unknown").notHas("empty.txt");
              }
            }).raises("dir 'test/unknown' must exist.");
          }
        });
      }
    });
  }
});