"use strict";

var _dogmalangmin = require("dogmalangmin");

const path = _dogmalangmin.dogma.use(require("dogmalang.path"));const fs = _dogmalangmin.dogma.use(require("fs"));
const $FileWrapper = class FileWrapper {
  constructor(...args) {
    {
      if ((0, _dogmalangmin.len)(args) == 0) {
        _dogmalangmin.dogma.raise("file expected.");
      }_dogmalangmin.dogma.update(this, { name: "path", visib: ":", assign: "::=", value: path.join(...args) });
    }
  }
};
const FileWrapper = new Proxy($FileWrapper, { apply(receiver, self, args) {
    return new $FileWrapper(...args);
  } });module.exports = exports = FileWrapper;
FileWrapper.prototype.exists = function () {
  {
    {
      const [ok, rslt] = _dogmalangmin.dogma.peval(() => {
        return fs.statSync(this._path);
      });if (!ok || !rslt.isFile()) {
        _dogmalangmin.dogma.raise("file '%s' must exist.", this._path);
      }
    }
  }return this;
};
FileWrapper.prototype.doesNotExist = FileWrapper.prototype.notExists = function () {
  {
    {
      const [ok, rslt] = _dogmalangmin.dogma.peval(() => {
        return fs.statSync(this._path);
      });if (ok && rslt.isFile()) {
        _dogmalangmin.dogma.raise("file '%s' must not exist.", this._path);
      }
    }
  }return this;
};
FileWrapper.prototype.includes = function (texts) {
  /* istanbul ignore next */_dogmalangmin.dogma.paramExpected("texts", texts, [_dogmalangmin.list, _dogmalangmin.any]);texts = (0, _dogmalangmin.list)(texts);{
    let cont;this.exists();cont = fs.readFileSync(this._path, "utf8");for (const t of texts) {
      if (!cont.includes((0, _dogmalangmin.text)(t))) {
        _dogmalangmin.dogma.raise("file '%s' must include '%s'.", this._path, t);
      }
    }
  }return this;
};
FileWrapper.prototype.doesNotInclude = FileWrapper.prototype.notIncludes = function (texts) {
  /* istanbul ignore next */_dogmalangmin.dogma.paramExpected("texts", texts, [_dogmalangmin.list, _dogmalangmin.any]);texts = (0, _dogmalangmin.list)(texts);{
    let cont;this.exists();cont = fs.readFileSync(this._path, "utf8");for (const t of texts) {
      if (cont.includes((0, _dogmalangmin.text)(t))) {
        _dogmalangmin.dogma.raise("file '%s' must not include '%s'.", this._path, t);
      }
    }
  }return this;
};
FileWrapper.prototype.isEmpty = function () {
  {
    this.exists();if (fs.statSync(this._path).size > 0) {
      _dogmalangmin.dogma.raise("file '%s' must be empty.", this._path);
    }
  }return this;
};
FileWrapper.prototype.isNotEmpty = function () {
  {
    this.exists();if (fs.statSync(this._path).size == 0) {
      _dogmalangmin.dogma.raise("file '%s' must not be empty.", this._path);
    }
  }return this;
};
FileWrapper.prototype.isJson = function () {
  {
    this.exists();{
      const [ok, err] = _dogmalangmin.dogma.peval(() => {
        return _dogmalangmin.json.decode(fs.readFileSync(this._path, "utf-8"));
      });if (!ok) {
        _dogmalangmin.dogma.raise("file '%s' must be JSON.", this._path);
      }
    }
  }return this;
};
FileWrapper.prototype.eq = function (cont) {
  /* istanbul ignore next */_dogmalangmin.dogma.paramExpected("cont", cont, _dogmalangmin.text);{
    this.exists();if (fs.readFileSync(this._path, "utf8") != cont) {
      _dogmalangmin.dogma.raise("'%s' file content must be '%s'.", this._path, cont);
    }
  }return this;
};
FileWrapper.prototype.neq = FileWrapper.prototype.ne = function (cont) {
  /* istanbul ignore next */_dogmalangmin.dogma.paramExpected("cont", cont, _dogmalangmin.text);{
    this.exists();if (fs.readFileSync(this._path, "utf8") == cont) {
      _dogmalangmin.dogma.raise("'%s' file content must be '%s'.", this._path, cont);
    }
  }return this;
};
FileWrapper.prototype.sameAs = function (path) {
  /* istanbul ignore next */_dogmalangmin.dogma.paramExpected("path", path, _dogmalangmin.text);{
    this.exists();if (fs.readFileSync(this._path, "utf8") != fs.readFileSync(path, "utf8")) {
      _dogmalangmin.dogma.raise("file '%s' must be same as file '%s'.", this._path, path);
    }
  }return this;
};
FileWrapper.prototype.notSameAs = function (path) {
  /* istanbul ignore next */_dogmalangmin.dogma.paramExpected("path", path, _dogmalangmin.text);{
    this.exists();if (fs.readFileSync(this._path, "utf8") == fs.readFileSync(path, "utf8")) {
      _dogmalangmin.dogma.raise("file '%s' must not be same as file '%s'.", this._path, path);
    }
  }return this;
};