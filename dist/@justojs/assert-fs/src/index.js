"use strict";

var _dogmalangmin = require("dogmalangmin");

const assert = _dogmalangmin.dogma.use(require("@justojs/assert"));const DirWrapper = _dogmalangmin.dogma.use(require("./DirWrapper"));const FileWrapper = _dogmalangmin.dogma.use(require("./FileWrapper"));
module.exports = exports = assert;
assert.dir = (...args) => {
  {
    return DirWrapper(...args);
  }
};
assert.file = (...args) => {
  {
    return FileWrapper(...args);
  }
};