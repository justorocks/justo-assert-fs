"use strict";

var _dogmalangmin = require("dogmalangmin");

const path = _dogmalangmin.dogma.use(require("dogmalang.path"));const fs = _dogmalangmin.dogma.use(require("fs"));
const $DirWrapper = class DirWrapper {
  constructor(...args) {
    {
      if ((0, _dogmalangmin.len)(args) == 0) {
        _dogmalangmin.dogma.raise("dir expected.");
      }_dogmalangmin.dogma.update(this, { name: "path", visib: ":", assign: "::=", value: path.join(...args) });
    }
  }
};
const DirWrapper = new Proxy($DirWrapper, { apply(receiver, self, args) {
    return new $DirWrapper(...args);
  } });module.exports = exports = DirWrapper;
DirWrapper.prototype.exists = function () {
  {
    {
      const [ok, res] = _dogmalangmin.dogma.peval(() => {
        return fs.statSync(this._path);
      });if (!ok || !res.isDirectory()) {
        _dogmalangmin.dogma.raise("dir '%s' must exist.", this._path);
      }
    }
  }return this;
};
DirWrapper.prototype.doesNotExist = DirWrapper.prototype.notExists = function () {
  {
    {
      const [ok, res] = _dogmalangmin.dogma.peval(() => {
        return fs.statSync(this._path);
      });if (ok && res.isDirectory()) {
        _dogmalangmin.dogma.raise("dir '%s' must not exist.", this._path);
      }
    }
  }return this;
};
DirWrapper.prototype.has = function (entries) {
  /* istanbul ignore next */_dogmalangmin.dogma.paramExpected("entries", entries, [_dogmalangmin.list, _dogmalangmin.text]);entries = (0, _dogmalangmin.list)(entries);{
    this.exists();for (const e of entries) {
      if (!_dogmalangmin.dogma.getItem(_dogmalangmin.dogma.peval(() => {
        return fs.accessSync(path.join(this._path, e));
      }), 0)) {
        _dogmalangmin.dogma.raise("dir '%s' must have entry '%s'.", this._path, e);
      }
    }
  }return this;
};
DirWrapper.prototype.doesNotHave = DirWrapper.prototype.notHas = function (entries) {
  /* istanbul ignore next */_dogmalangmin.dogma.paramExpected("entries", entries, [_dogmalangmin.list, _dogmalangmin.text]);entries = (0, _dogmalangmin.list)(entries);{
    this.exists();for (const e of entries) {
      if (_dogmalangmin.dogma.getItem(_dogmalangmin.dogma.peval(() => {
        return fs.accessSync(path.join(this._path, e));
      }), 0)) {
        _dogmalangmin.dogma.raise("dir '%s' must not have entry '%s'.", this._path, e);
      }
    }
  }return this;
};